import React from 'react';
import { connect } from 'react-redux';
import axios from 'axios';
import { Card, CardHeader } from 'material-ui/Card';
import Paper from 'material-ui/Paper';
import Subheader from 'material-ui/Subheader';
import { requestPatients, receivePatients } from '../actions/patientsActions';
import { CONTEXT_ROOT } from '../constants/RouterConstants';

class PatientsContent extends React.Component {
	componentDidMount() {
		this.fetchPatients(1271);
	}

	fetchPatients(patientId) {
		this.props.onRequestPatients();
		return axios
			.get(CONTEXT_ROOT + '/api/v5/patient', {
				name: patientId,
				client_id: 'imitoMobile'
			})
			.then(response => {
				this.props.onReceivePatients(response.data);
			})
			.catch(error => {
				console.log(JSON.stringify(error.stack));
			});
	}

	render() {
		return (
			<div>
				<Paper>
					<Subheader>Patients</Subheader>
					<Card>
						<CardHeader
							title="Patient 1"
							subtitle="Condition"
							avatar={CONTEXT_ROOT + '/img/img-imito-logo.png'}
						/>
					</Card>
				</Paper>
			</div>
		);
	}
}

const mapDispatchToProps = dispatch => {
	return {
		onReceivePatients: patientId => {
			dispatch(receivePatients(patientId));
		},
		onRequestPatients: () => {
			dispatch(requestPatients());
		}
	};
};

const mapStateToProps = state => {
	return {
		timeline: state.timeline
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(PatientsContent);
