import React from 'react';
import { CONTEXT_ROOT } from '../constants/RouterConstants';

class ErrorPageContent extends React.Component {
	render() {
		return (
			<div className="height-100 error-page-content">
				<div className="width-100" style={{ textAlign: 'left' }}>
					<img
						src={CONTEXT_ROOT + '/img/img-imito-logo.png'}
						alt="Imito logo"
					/>
				</div>
				<div style={{ height: '25%' }} />
				<div className="error-code">404</div>
				<div className="error-page-text">Page not found</div>
				<div>
					<a
						href={CONTEXT_ROOT + '/app/patients'}
						type="button"
						className="btn btn-imito">
						Back to ImitoAdmin
					</a>
				</div>
			</div>
		);
	}
}

export default ErrorPageContent;
