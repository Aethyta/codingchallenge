import React from 'react';
import axios from 'axios';
import qs from 'qs';
import {
	userLoaded,
	userLoggedIn,
	loginFailed,
	clearErrorMessage,
	amendLoginData
} from '../actions/userActions';
import { connect } from 'react-redux';
import { Checkbox, TextField } from 'material-ui';
import { CONTEXT_ROOT } from '../constants/RouterConstants';

class LoginComponent extends React.Component {
	login = event => {
		event.preventDefault();
		let login = this.props.login;
		let dispatcher = this.props;
		if (!login.username || !login.password) {
			dispatcher.onLoginFailed('Input username and password');
		} else {
			axios
				.post(
					CONTEXT_ROOT + '/api/v5/auth',
					qs.stringify({
						grant_type: 'password',
						username: login.username,
						password: login.password,
						client_id: 'imitoMobile',
						uuid: '00000000-0000-0000-0000-000000000000',
						isHandlingMultipleGroups: 'true'
					})
				)
				.then(response => {
					dispatcher.onUserLoggedIn();
					return response.data;
				})
				.then(userInformation => dispatcher.onUserLoaded(userInformation))
				.then(dispatcher.history.push('app/patients'))
				.catch(error => {
					dispatcher.onLoginFailed('Oops! Something went wrong.');
					console.log(JSON.stringify(error.stack));
				});
		}
	};

	handleInputChange = event => {
		const target = event.target;
		const value = target.type === 'checkbox' ? target.checked : target.value;
		const name = target.name;
		this.props.onAmendLoginData(name, value);
		this.props.onClearErrorMessage();
	};

	render() {
		return (
			<div className="height-100">
				<div className="height-100" style={{ position: 'relative' }}>
					<div className="height-100 login-left-panel white-content">
						<div className="padded-content">
							<div>
								<img
									src={CONTEXT_ROOT + '/img/img-imito-logo.png'}
									alt="Imito Logo"
								/>
							</div>
							<div className="empty-divider" style={{ height: '4em' }} />
							<div className="login-header">
								Welcome <span className="text-imito-bold">imitoConnect</span>{' '}
								admin panel.
							</div>
							<div className="login-text">
								{'Please enter your hospitals credentials to sign in'}
							</div>

							<form onSubmit={this.login}>
								<div>
									<TextField
										id="username"
										floatingLabelText="Username"
										name="username"
										floatingLabelStyle={{ color: 'rgba(226, 53, 42, 0.5)' }}
										onChange={this.handleInputChange}
									/>
								</div>
								<div>
									<TextField
										id="password"
										floatingLabelText="Password"
										name="password"
										floatingLabelStyle={{ color: 'rgba(226, 53, 42, 0.5)' }}
										type="password"
										onChange={this.handleInputChange}
									/>
								</div>
								{this.props.login.errorMessage && (
									<div className="error-msg">
										{this.props.login.errorMessage}
									</div>
								)}
								<div>
									<Checkbox checked={this.props.checked} label="Remember me" />
								</div>
								<div className="empty-divider" style={{ height: '4em' }} />
								<div>
									<button
										className="btn btn-imito btn-imito-wide"
										type="submit">
										Sign in
									</button>
								</div>
							</form>
						</div>
					</div>
					<div className="height-100 login-right-panel" />
				</div>
			</div>
		);
	}
}

const mapDispatchToProps = dispatch => {
	return {
		onUserLoggedIn: () => {
			dispatch(userLoggedIn());
		},
		onUserLoaded: user => {
			dispatch(userLoaded(user));
		},
		onLoginFailed: errorMessage => {
			dispatch(loginFailed(errorMessage));
		},
		onClearErrorMessage: () => {
			dispatch(clearErrorMessage());
		},
		onAmendLoginData: (username, password) => {
			dispatch(amendLoginData(username, password));
		}
	};
};

const mapStateToProps = state => {
	return {
		login: state.login
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(LoginComponent);
