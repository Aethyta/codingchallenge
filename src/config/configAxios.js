import * as axios from 'axios';
import qs from 'qs';
import UserService from '../service/UserService';
import { CONTEXT_ROOT } from '../constants/RouterConstants';

var currentRequestFuture = null;
localStorage.setItem('accessTokenRequest', null);

const configAxios = (store, history) => {
	axios.interceptors.request.use(
		function(config) {
			if (
				config.url.indexOf('login') > 0 ||
				config.url.indexOf('auth') > 0 ||
				config.url.indexOf('info') > 0
			) {
				return config;
			}
			let queryObj = qs.parse(config.data);
			let user = JSON.parse(localStorage.getItem('user'));
			let accessToken = '';
			if (user && user.access_token) {
				accessToken = user.access_token;
				queryObj.access_token = accessToken;
				config.url += '?' + qs.stringify(queryObj);
			}
			return config;
		},
		function(error) {
			return Promise.reject(error);
		}
	);
	axios.interceptors.response.use(
		function(response) {
			return response;
		},
		function(error) {
			if (error.response && 401 === error.response.status) {
				if (error.response.data.error === 'unauthorized') {
					window.location.href(CONTEXT_ROOT + '/login');
					localStorage.removeItem('user');
				}

				// token refresh logic
				if (error.response.data.error === 'invalid_token') {
					var now = Date.now();
					if (!localStorage.getItem('accessTokenRequest')) {
						localStorage.setItem('accessTokenRequest', now);
						currentRequestFuture = new UserService()
							.refreshToken(
								JSON.parse(localStorage.getItem('user')).refresh_token
							)
							.then(function(response) {
								var lastAccessTokenRequest = localStorage.getItem(
									'accessTokenRequest'
								);
								if (lastAccessTokenRequest === String(now)) {
									localStorage.setItem('user', JSON.stringify(response.data));
									localStorage.setItem('accessTokenRequest', null);
								}
								var pos = error.config.url.indexOf('access_token=');
								if (pos > 0) {
									error.config.url = error.config.url.substring(0, pos - 1);
								}
								return axios.request(error.config);
							})
							.catch(function(error) {
								window.location.href(CONTEXT_ROOT + '/login');
								localStorage.removeItem('user');
								localStorage.setItem('accessTokenRequest', null);
								return axios.request(error.config);
							});
					} else {
						currentRequestFuture = currentRequestFuture.then(function() {
							var pos = error.config.url.indexOf('access_token=');
							if (pos > 0) {
								error.config.url = error.config.url.substring(0, pos - 1);
							}
							return axios.request(error.config);
						});
					}
					return currentRequestFuture;
				}
				// token refresh logic ends
			} else {
				return Promise.reject(error);
			}
		}
	);
};

export default configAxios;
