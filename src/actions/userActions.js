export const USER_LOGGED_IN = 'USER_LOGGED_IN';
export const USER_LOGGED_OUT = 'USER_LOGGED_OUT';
export const USER_LOADED = 'USER_LOADED';
export const LOGIN_FAILED = 'LOGIN_FAILED';
export const CLEAR_ERROR_MESSAGE = 'CLEAR_ERROR_MESSAGE';
export const AMEND_LOGIN_DATA = 'AMEND_LOGIN_DATA';

export function userLoggedIn() {
	return {
		type: USER_LOGGED_IN
	};
}

export function userLoggedOut() {
	return {
		type: USER_LOGGED_OUT
	};
}

export function userLoaded(user) {
	return {
		type: USER_LOADED,
		payload: user
	};
}

export function loginFailed(errorMessage) {
	return {
		type: LOGIN_FAILED,
		payload: errorMessage
	};
}

export function clearErrorMessage() {
	return {
		type: CLEAR_ERROR_MESSAGE
	};
}

export function amendLoginData(name, value) {
	return {
		type: AMEND_LOGIN_DATA,
		payload: {
			name,
			value
		}
	};
}
