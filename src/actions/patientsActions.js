export const REQUEST_PATIENTS = 'REQUEST_PATIENTS';
export const RECEIVE_PATIENTS = 'RECEIVE_PATIENTS';

export function requestPatients() {
	return {
		type: REQUEST_PATIENTS
	};
}

export function receivePatients(patients) {
	return {
		type: RECEIVE_PATIENTS,
		patients
	};
}
