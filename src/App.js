import React from 'react';
import { connect } from 'react-redux';
import './App.css';
import LeftPanel from './view/LeftPanel';
import PatientsContent from './components/PatientsContent';

class App extends React.Component {
	userState = { loggedIn: false };
	render() {
		return (
			<div>
				<div className="height-100 app-left-panel">
					<LeftPanel user={this.props.login.user} />
				</div>
				<div className="height-100 app-right-panel half-padded-content">
					<PatientsContent />
				</div>
			</div>
		);
	}
}

const mapStateToProps = state => {
	return {
		login: state.login,
		timeline: state.timeline
	};
};

export default connect(mapStateToProps)(App);
