import {
	USER_LOGGED_IN,
	USER_LOGGED_OUT,
	USER_LOADED,
	LOGIN_FAILED,
	CLEAR_ERROR_MESSAGE,
	AMEND_LOGIN_DATA
} from '../actions/userActions';

const userLoggedIn = localStorage.getItem('user') ? true : false;
const initialState = {
	username: JSON.parse(localStorage.getItem('user')),
	password: '',
	loggedIn: userLoggedIn,
	checked: true,
	errorMessage: ''
};

export function userReducer(state = initialState, action) {
	switch (action.type) {
		case USER_LOGGED_IN:
			return {
				...state,
				loggedIn: true
			};
		case USER_LOGGED_OUT:
			return {
				...state,
				loggedIn: false
			};
		case USER_LOADED:
			return {
				...state,
				user: action.payload
			};
		case LOGIN_FAILED:
			return {
				...state,
				loggedIn: false,
				errorMessage: action.payload
			};
		case CLEAR_ERROR_MESSAGE:
			return {
				...state,
				loggedIn: false,
				errorMessage: ''
			};
		case AMEND_LOGIN_DATA:
			return {
				...state,
				[action.payload.name]: action.payload.value
			};
		default:
			return state;
	}
}
