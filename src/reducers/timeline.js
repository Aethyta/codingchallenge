import { REQUEST_PATIENTS, RECEIVE_PATIENTS } from '../actions/patientsActions';

const initialState = {
	loading: false,
	patients: []
};

export function patientsReducer(state = initialState, action) {
	switch (action.type) {
		case REQUEST_PATIENTS:
			return {
				...state,
				loading: true
			};
		case RECEIVE_PATIENTS:
			return {
				...state,
				patients: action.patients
			};
		default:
			return state;
	}
}
