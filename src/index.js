import React from 'react';
import ReactDOM from 'react-dom';
import { createStore, compose, combineReducers, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { Provider } from 'react-redux';
import configAxios from './config/configAxios';
import { MuiThemeProvider } from 'material-ui';
import { createLogger } from 'redux-logger';
import { CONTEXT_ROOT } from './constants/RouterConstants';
import { userReducer } from './reducers/login';
import { patientsReducer } from './reducers/timeline';
import LoginComponent from './components/LoginComponent';
import ErrorPageContent from './components/ErrorPageContent';
import App from './App';

let browserHistory = BrowserRouter.history;
const loggerMiddleware = createLogger({});
let middleware = [thunk, loggerMiddleware];

let store = createStore(
	combineReducers({
		login: userReducer,
		timeline: patientsReducer
	}),
	{},
	compose(applyMiddleware(...middleware))
);
configAxios(store, browserHistory);

ReactDOM.render(
	<MuiThemeProvider>
		<Provider store={store}>
			<BrowserRouter basename={CONTEXT_ROOT} history={this.browserHistory}>
				<Switch>
					<Route
						path={'/login'}
						component={LoginComponent}
						history={this.browserHistory}
					/>
					<Route path={'/app/patients'} component={App} />
					<Route component={ErrorPageContent} />
				</Switch>
			</BrowserRouter>
		</Provider>
	</MuiThemeProvider>,
	document.getElementById('root')
);
