import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class LeftMenu extends Component {
	render() {
		return (
			<div className="left-menu">
				<Link to="/app/patients">
					<div>Patients</div>
				</Link>
			</div>
		);
	}
}

export default LeftMenu;
