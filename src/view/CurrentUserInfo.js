import React, { Component } from 'react';
import { connect } from 'react-redux';
import UserService from '../service/UserService';
import { userLoggedOut } from '../actions/userActions';
import { withRouter } from 'react-router-dom';
import { CONTEXT_ROOT } from '../constants/RouterConstants';

class CurrentUserInfo extends Component {
	logout = () => {
		new UserService()
			.logout()
			.then(response => {
				this.props.store.dispatch(userLoggedOut(response.data));
				localStorage.removeItem('user');
				this.props.history.push(CONTEXT_ROOT + '/login');
			})
			.catch(error => {
				console.error('Error when logging out: ' + error);
				this.props.history.push(CONTEXT_ROOT + '/login');
			});
	};

	render() {
		return (
			<div className="">
				<div className="user-capitals">
					{this.props.user && this.props.user.user && this.props.user.user.user
						? this.props.user.user.user.givenName.substring(0, 1) +
						  ' ' +
						  this.props.user.user.user.familyName.substring(0, 1)
						: ''}
				</div>
				<div style={{ marginLeft: '6em', paddingTop: '0.5em' }}>
					<div className="user-details-name ">
						{this.props.user && this.props.user.user
							? this.props.user.user.user.givenName +
							  ' ' +
							  this.props.user.user.user.familyName
							: ''}
					</div>
					<div className="blended-text">
						<button className="btn-to-text" type="button" onClick={this.logout}>
							LOGOUT
						</button>
					</div>
				</div>
			</div>
		);
	}
}

const mapStateToProps = state => {
	return {
		user: state.user
	};
};

export default withRouter(connect(mapStateToProps)(CurrentUserInfo));
