import React, { Component } from 'react';
import CurrentUserInfo from './CurrentUserInfo';
import LeftMenu from './LeftMenu';
import { CONTEXT_ROOT } from '../constants/RouterConstants';

class LeftPanel extends Component {
	render() {
		return (
			<div className="half-padded-content left-panel height-100">
				<div>
					<img
						src={CONTEXT_ROOT + '/img/img-imito-logo-white.png'}
						alt="Imito logo"
					/>
				</div>
				<div className="empty-divider" style={{ height: '4em' }} />
				<LeftMenu />
				<div className="empty-divider" style={{ height: '8em' }} />
				<div style={{ height: '3em' }} />
				<div style={{ height: '3em' }} />
				<CurrentUserInfo user={this.props.user} />
			</div>
		);
	}
}

export default LeftPanel;
