import React from 'react';
import InfoService from '../service/InfoService';
import { CONTEXT_ROOT } from '../constants/RouterConstants';

class CurrentVersionInfo extends React.Component {
	constructor() {
		super();
		this.setState({
			version: {
				version: '',
				restVersions: []
			}
		});
	}
	componentDidMount() {
		new InfoService()
			.getInfo()
			.then(function(response) {
				this.setState({ version: response.data });
			})
			.catch(function() {
				console.error('Cannot get version info!');
				window.location.href(CONTEXT_ROOT + '/login');
			});
	}
	render() {
		return (
			<div className="stats-panel">
				<div className="header">VERSION</div>

				<div className="row">
					<div className="col-sm-5">imitoConnect:</div>
					<div className="col-sm-7 left-panel-value">
						{this.state.version.version}
					</div>
				</div>
				<hr />
				<div className="row">
					<div className="col-sm-5">REST API:</div>
					<div className="col-sm-7 left-panel-value">
						{this.state.version.restVersions.join(', ')}
					</div>
				</div>
			</div>
		);
	}
}

export default CurrentVersionInfo;
