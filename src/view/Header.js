import React from 'react';

class Header extends React.Component {
	constructor() {
		super();
	}

	render() {
		return (
			<div>
				<div className="row">
					<div className="col-lg-8 header-big">{this.props.headerValue}</div>
					<div className="col-lg-4">
						{this.props.btnText && (
							<button
								className="btn btn-imito-wider btn-imito"
								onClick={() => this.props.onBtnClick()}>
								{this.props.btnText}
							</button>
						)}
						{this.props.smallBtnText && (
							<button
								className="btn btn-imito-small"
								onClick={() => this.props.onSmallBtnClick()}
								style={{ marginTop: '10px' }}>
								{this.props.smallBtnText}
							</button>
						)}
					</div>
				</div>
				<div className="opacity-50">{this.props.subheaderValue}</div>
				{!this.props.noHr && <hr style={{ borderTopColor: 'black' }} />}
			</div>
		);
	}
}

export default Header;
