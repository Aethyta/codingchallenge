import axios from 'axios';
import qs from 'qs';
import { BACKEND_URL } from '../constants/RouterConstants';

class UserService {
	login(username, password) {
		return axios.post(
			BACKEND_URL + 'api/v5/auth',
			qs.stringify({
				grant_type: 'password',
				username: username,
				password: password,
				client_id: 'imitoMobile',
				uuid: '00000000-0000-0000-0000-000000000000'
			})
		);
	}

	logout() {
		return axios.get(BACKEND_URL + 'api/v5/logout');
	}

	refreshToken(refreshToken) {
		return axios.post(
			'/oauth/token',
			qs.stringify({
				refresh_token: refreshToken,
				client_id: 'imitoAdmin',
				grant_type: 'refresh_token'
			}),
			{
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				}
			}
		);
	}

	getUserList(namePart) {
		return axios.get(BACKEND_URL + 'api/v5/users', {
			params: {
				namePart: namePart
			}
		});
	}
}

export default UserService;
