**Goal:** Create a webapp prototype in a timeframe of around 3 hours with React to let the user login (username: gh) to the system and show a timeline of one specific patient.
**Reference:** current imitoCam iOS timeline (see screenshots)
Code which can be reused: imitoAdmin login panel prototype (bitbucket repo). 
imitoAdmin: https://imitoconnect.imito.io:6443/imitoAdmin
Login: gh
Password: iRokk!!

No styling required. Layout is up to the developer.

API Documentation in Swagger: https://ci.imito.io/openapi/swagger.yaml
Backend API to use: https://imitoconnect.imito.io:6443/api
Login: gh
Password: iRokk!!
imitoPatientId: 1271
Please use v5 of the API, such as:
https://imitoconnect.imito.io:6443/api/v5/patients
https://imitoconnect.imito.io:6443/api/v5/timeline

**Show on the timeline**
1. Basic patient info (first name, last name, id, date of birth, sex)
2. List of all series which were taken for this patient. Each item contains:
3. Timestamp (series creation time)
4. Author
5. Thumbnails of the series media

**If there is some time left:** 
* List of hashtags of the series
* Simple life client-side search by hashtags through the series list